const menu = [
              {
                gambar:"foto/krakatau burst.png",
                nama:"Krakatau Burst",
                harga: 68000
              },
              {
                gambar:"foto/black meat monsta.jpg",
                nama:"Black Meat Monsta",
                harga: 68000
              },
              {
                gambar:"foto/super supreme.jpg",
                nama:"Super Supreme",
                harga: 60000
              },
              {
                gambar:"foto/american favorite.png",
                nama:"American Favorite",
                harga: 68000
              },
              {
                gambar:"foto/chocolate waffle.jpg",
                nama:"Chocolate Waffle",
                harga: 35000
              },
              {
                gambar:"foto/baked eggtato.jpg",
                nama:"Baked Eggtato",
                harga: 35000
              },
              {
                gambar:"foto/smoked beef and egg.jpg",
                nama:"Smoked Beef and Egg",
                harga: 35000
              },
              {
                gambar:"foto/sunny side up.jpg",
                nama:"Sunny Side Up",
                harga: 25000
              },
              {
                gambar:"foto/blue ocean.jpg",
                nama:"Blue Ocean",
                harga: 30000
              },
              {
                gambar:"foto/lychee breeze.jpg",
                nama:"Lychee Breeze",
                harga: 30000
              },
              {
                gambar:"foto/tropical punch.jpg",
                nama:"Tropical Punch",
                harga: 30000
              },
              {
                gambar:"foto/mixberry.jpg",
                nama:"Mix Berry",
                harga: 30000
              }
            ];

const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((acc) => {
    return acc + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}



const callbackMap = (item, index, array)=>{
    const elmnt = document.querySelector('.menu-makanan');
  
    elmnt.innerHTML +=`
                    <div class="container col-md-3 mb-4" style="float:left; display:flex;">
                      <div class="card" style="width: 20rem;">
                        <img src="${item.gambar}" class="card-img-top" alt="..." height="250px">
                        <div class="card-body">
                          <h5 class="card-title">${item.nama}</h5>
                          <p class="card-text">Harga: Rp${item.harga}</p>
                          <a href="#" class="btn btn-warning">Buy Now</a>
                        </div>
                      </div>
                  </div>
                  `
}


menu.map(callbackMap);
jumlahMENU(menu);

const buttonElmnt =document.querySelector('.button-cari');

buttonElmnt.addEventListener('click', ()=>{
  const hasilPencarian = menu.filter((item, index)=>{
                        const inputElmnt  = document.querySelector('.input-keyword');
                        const keyword  = inputElmnt.value.toLowerCase();
                        const namaItem    = item.nama.toLowerCase();

                        return namaItem.includes(keyword);
                      })
    document.querySelector('.menu-makanan').innerHTML = '';

    hasilPencarian.map(callbackMap);
    jumlahMENU(hasilPencarian);
});
